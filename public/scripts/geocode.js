//https://nominatim.openstreetmap.org/search?q=135+pilkington+avenue,+birmingham&format=xml&polygon=1&addressdetails=1

var accessToken = 'pk.eyJ1Ijoid2ViYm1hIiwiYSI6ImNqamI3b2VrNzM5Y2YzcW1uczYyNDZyNm4ifQ.7PmccZeHpu8hpW2c1GpHwA'; 
        
// Определяем компонент - опция для списка найденных адресов при поиске по названию
Vue.component('address-option', {  
  props: ['option'],
  template: `<div class="a-option" v-on:click="saySelected">{{ option.title }} </div>`,
  methods: {
    //излучает событие, что конкретный вариант адреса был выбран
    saySelected: function(event){      
      this.$emit('option-is-selected',this.option.position)
    }
  }
})   

// Определяем компонент - поле поиска адреса по не строгому названию объекта.
Vue.component('address-select', {  
  props: ['placeholder','lng','lat','id'],
  data: function () {
    return {
      start: '', //'Москва',
      options: [
        /*{title: 'Екатеринбург', position: 0, coordinates: {lng: 56; lat: 56.1}},
        {title: 'Питербург', position: 1, coordinates: {lng: 55; lat: 56.1}},
        {title: 'Москва', position: 2, coordinates: {lng: 55; lat: 54.1}}*/
      ], 
      visible: true, //видимость списка вариантов точного адреса
      timer: null   //таймер задержки, чтобы отправлять запрос только после ввода куска текста
    }
  },
  template: 
    `<div class="user-input">      
        <input 
            v-model="start" type="text" 
            v-on:input="askForSearch" 
            v-on:keyup="askForSearch" 
            v-on:change="askForSearch" 
            v-bind:placeholder="placeholder" 
            v-bind:lng="lng" v-bind:lat="lat"
            v-bind:id="id"></br>      
      <address-option
         v-for="option in options" 
         v-bind:option="option"         
         v-on:option-is-selected="fixTheChoise($event)"
         v-if="visible"
      ></address-option>
    </div>`,
    methods: {
      //Фиксирует выбор пользователя
      fixTheChoise: function(position){          
          this.start = this.options[position].title;
          this.lng = this.options[position].coordinates.lng;
          this.lat = this.options[position].coordinates.lat;
          this.visible = false; //скрываем список с вариантами
      },

      //Декодирует то, что ввел пользователь в точный адрес с ширтой и долготой
      askForSearch: function(event){
        var t = this;

        /*организуем задержку, чтобы не отправлять запрос после каждой введенной буквы,
        а отправить в паузу после ввода куска текста, предположительно слова целиком */
        if (this.timer) {
          clearTimeout(this.timer);
          this.timer = null;
        }
        this.timer = setTimeout(() => {
          //запрос на сервер за вариантами адреса
          console.log('запросим сервер');      
          
          if (this.start) { //если запрос не пустой
          
            var query = this.start.replace(/\s+/g,' ').trim().replace(/\s+/g,'+');
           
            //Запрос за маршрутом   TODO: Перенести на backend     
            axios.get('https://nominatim.openstreetmap.org/search?q='+query+'&format=json&polygon=1&addressdetails=1')
             .then(function (response) { 
                console.log(response.data);  
                
                t.options = [];
                if (response.data.length > 0){ //если что-нибудь нашлось
                    //наполняем список выбора из найденных вариантов                    
                    response.data.forEach(function(elem, i, arr){
                      t.options.push({
                        title: elem.display_name,
                        position: i,
                        coordinates: {lng: elem.lon, lat: elem.lat}
                      })
                    })
                } else {
                    //сообщение о неуспехе поиска
                    t.options.push({
                        title: 'Адрес не найден...',
                        position: 0,
                        coordinates: {lng: 0, lat: 0}
                    })
                }

                t.visible = true; // проявим список найденных вариантов              
             })
             .catch(function (error) { // если что-то пошло не так...
               console.log(error);
             })
           }

        }, 800);
        
      }
    }
})   



