mapboxgl.accessToken = 'pk.eyJ1Ijoid2ViYm1hIiwiYSI6ImNqamI3b2VrNzM5Y2YzcW1uczYyNDZyNm4ifQ.7PmccZeHpu8hpW2c1GpHwA';
var map = new mapboxgl.Map({
	container: 'map',
	style: 'mapbox://styles/mapbox/streets-v10',
	center: [60.65400, 56.84120],
	zoom: 14
});

// Добавим кнопки управления масштабом и вращением карты
var nav = new mapboxgl.NavigationControl({
    showCompass: true,
    showZoom: true
});
map.addControl(nav, 'bottom-right');

// Действия после загрузки карты
map.on('load', function () {
    map.resize(); //иначе карта будет 300px высотой, пока руками не потянешь окно
});

/*
mapbox://styles/mapbox/light-v9
mapbox://styles/mapbox/dark-v9
mapbox://styles/mapbox/satellite-v9
mapbox://styles/mapbox/satellite-streets-v10
mapbox://styles/mapbox/navigation-preview-day-v2
mapbox://styles/mapbox/navigation-preview-night-v2
mapbox://styles/mapbox/navigation-guidance-day-v2
mapbox://styles/mapbox/navigation-guidance-night-v2

*/
