var routeId = "route"; //id для слоя с текущим маршрутом

//шаблон входного обекта с данными для функции map.addlayer
var routeTemplate = {
    "id": routeId,
    "type": "line",
    "source": {
        "type": "geojson",
        "data": {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    // [-122.48369693756104, 37.83381888486939],
                    // [-122.48348236083984, 37.83317489144141],
                    // [-122.48339653015138, 37.83270036637107]
                ]
            }
        }
    },
    "layout": {
        "line-join": "round",
        "line-cap": "round"
    },
    "paint": {
        "line-color": "#888",
        "line-width": 8
    }
}


//панель управления сервисом
var interface = new Vue({
  el: '#interface',
  data: {
    accessToken: accessToken,    
    inputData: [{ //исходные данные для полей откуда-куда
            placeholder: 'Откуда едем', 
            id: Math.round(Math.random()*100000000),
            lng: 60.6541015, 
            lat: 60.6541015
        }, {
            placeholder: 'Куда едем', 
            id: Math.round(Math.random()*100000000),
            lng: 60.6649360, 
            lat: 56.8380080
        }]
  },
  methods: {
    doroute: function (event) {
      var t = this;
      
      //Переводим из строкового представления в массивы
      var startInput = document.getElementById(t.inputData[0].id);
      var startPoint = [parseFloat(startInput.getAttribute("lng")), parseFloat(startInput.getAttribute("lat"))];   
      //var finishPoint = [t.inputData[1].lng, t.inputData[1].lat]; //this.finish.split(",").map(function(item){return parseFloat(item)});
      var finishInput = document.getElementById(t.inputData[1].id);
      var finishPoint = [parseFloat(finishInput.getAttribute("lng")), parseFloat(finishInput.getAttribute("lat"))];   
      console.log('startPoint',startPoint)
      console.log('finishPoint',finishPoint)
            
      //Запрос за маршрутом
      //axios.get('https://api.mapbox.com/directions/v5/mapbox/driving/-73.989,40.733;-74,40.733?approaches=unrestricted;curb&access_token=pk.eyJ1Ijoid2ViYm1hIiwiYSI6ImNqamI3b2VrNzM5Y2YzcW1uczYyNDZyNm4ifQ.7PmccZeHpu8hpW2c1GpHwA')
      axios.get('https://api.mapbox.com/directions/v5/mapbox/cycling/'+startPoint.join(',')+';'+finishPoint.join(',')+'.json?access_token='+this.accessToken)
		.then(function (response) { 
            //console.log(response);
			var geometry = response.data.routes[0].geometry;
			var coordinates = polyline.decode(geometry)
			coordinates = coordinates.map(function(item){return [item[1],item[0]]}) //переставляем местами координаты
			//console.log('coordinates',coordinates)

			routeData = Object.assign({}, routeTemplate);
			routeData.source.data.geometry.coordinates = coordinates;
			//console.log('routeData',routeData)
          
			//наносим маршрут на карту
			if (map.getLayer(routeId) !== undefined) { //удалим прежний маршрут, если есть
				map.removeLayer(routeId);
				map.removeSource(routeId);
			}
			map.addLayer(routeData);
          
            //центруем карту на середине между стартом и финишем
            var midPoint = { 
              lng: (startPoint[0] + finishPoint[0])/2, 
              lat:(startPoint[1] + finishPoint[1])/2
            }
            map.setCenter(midPoint);
          
            //перемасштабируем карту, чтобы маршурт был виден целиком
            var newZoom = sizeToZoom({ 
              lng: Math.abs(startPoint[0] - finishPoint[0]), 
              lat: Math.abs(startPoint[1] - finishPoint[1]) 
            })            
            map.setZoom(newZoom);

		})
		.catch(function (error) { //если что-то пошло не так...
			console.log(error);
		})
		.then(function () { // при любом раскладе ...
		});

    }
  }
})

/* На входе объект с размерами по долготе и широте { lng: 56.5457645, lat: 56.8439025 }
   @return цифру масштаба
*/
function sizeToZoom(sizes){
    /*Экспериментальные данные:
     Размах                Масштаб
     0.0000100000000000    22.0
     0.0001000000000000    20.2
     0.0010000000000000    15.8
     0.0108344999999943    14.0
     0.1000000000000000    10.0
     0.9993983000000030     6.5
     1.5000000000000000     6.1
     2.0000000000000000     5.6
     3.0000000000000000     5.0	
     5.0058945000000000     4.3
     7.0000000000000000     3.9
    10.0000000000000000     3.4
    20.0000000000000000     2.2
    40.0000000000000000     1.5
    
    Эта зависимость неплохо апроксимируется уравнением
    zoom(size) = -1.3997275724*ln(size)+6.6356985357
    */
    
    s = Math.max(sizes.lat, sizes.lng);    
    return (-1.3997275724*Math.log(s)+6.6356985357)*1.3
}