'use strict'

// long stack trace (+clarify from co) при условии соотв. переменной среды
if (process.env.TRACE) {
  require('./libs/trace');
}

const koa = require('koa');
const app = new koa();

const config = require('config');
const convert = require('koa-convert');

// keys for in-koa KeyGrip cookie signing (used in session, maybe other modules)
app.keys = [config.secret];

let path = require('path');
let fs = require('fs');

//добавляем в koа-каскад нужных посредников
app.use(convert(require('./middlewares/favicon')));
app.use(convert(require('./middlewares/static')));
app.use(convert(require('./middlewares/logger')));
app.use(convert(require('./middlewares/errors')));
app.use(convert(require('./middlewares/session')));
app.use(convert(require('./middlewares/bodyParser')));
app.use(convert(require('./middlewares/multipartParser')));

//движок шаблонов EJS
const koaEjsTempl = require('koa-ejs');

koaEjsTempl(app, {
  root: path.join(config.root, 'templates'),
  layout: false,
  viewExt: 'ejs',
  cache: false,
  debug: true
});

// ---------------------------------------

var Router = require('koa-router');

var router = new Router(); //Instantiate the router

router.get('/', async function (ctx, next) {
  await ctx.render('index', {
      user: 'Петр',
      view_count: 1
  });
})

app
  .use(router.routes())
  .use(router.allowedMethods());
app.listen(3001);
