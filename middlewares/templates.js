// initialize template system early, to let error handler use them
// koa-views is a wrapper around many template systems!
// most of time it's better to use the chosen template system directly
var koaEjs = require('koa-ejs');
var config = require('config');
var path = require('path');

koaEjs(app, {
  root: path.join(config.root, 'templates'),
  //layout: 'template',
  viewExt: 'ejs',
  cache: false,
  debug: true
});

module.exports = koaEjs;
